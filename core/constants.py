PROJECT_NAME = "Gaussino MetaHEP"
PROJECT_DESCRIPTION = """
        Gaussino MetaHEP for training and retraining
        ML models using CaloChallenge setup for fast
        simulations.
    """
PROJECT_VERSION = "0.1.0"
ACTIVE_MODEL_NAMES = [
    "test",
    "vae",
]
POSSIBLE_ACTIONS = [
    "train",
    "generate",
    "convert",
    "evaluate",
]
